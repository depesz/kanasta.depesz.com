DEBUG = True
SECRET_KEY = 'whatever'
HOST = '0.0.0.0'
PORT = 26653
DB_DATABASE = 'depesz_kanasta'
DB_PORT = 5130
DB_HOST = '127.0.0.1'
DB_USER = 'depesz_kanasta'
DB_PASSWORD = 'anything'
LOG_FILENAME = 'log/kanasta.log'
APP_DIR = '/tmp'

# vim: set ft=python:
