# kanasta.depesz.com

## Disclaimer

This is my first non-trivial program in [Python](http://python.org/). I also choose to learn [Flask](http://flask.pocoo.org/) when doing this. So this is double-first-timer. As such, it definitely contains bad style, stupid constructs, and so on. But one has to learn with something.

## Description

I do like to play [Canasta](http://en.wikipedia.org/wiki/Canasta) game. Usually we play in the same group, and after some time we started tracking simple stats (number of wins) in Google docs.

This worked fine, but we decided it would be cooler to track more stats, and make it smarter. Plus I'm trying to remove my dependency on Google.

So, I decided to write this.

Its core it's very simple: track games where each game has two pairs of players. Do some simple calculations, draw some graphs, track some top-10 stats. And make it easy to use.
