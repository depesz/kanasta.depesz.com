#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, g, render_template, flash, session, request, redirect, url_for, make_response, send_from_directory
from functools import wraps
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.dates import DateFormatter
from matplotlib.figure import Figure
from kanasta import model
import crypt
import io
import logging
import logging.handlers
import random
import os

# Default configuration
DEBUG = True
SECRET_KEY = 'dev key'
HOST = '0.0.0.0'
DB_DATABASE = 'kanasta'
DB_PORT = 5920
DB_HOST = '/tmp'
DB_USER = 'kanasta'
LOG_FILENAME = 'log/kanasta.log'
MAIL_SERVER = '127.0.0.1'
MAIL_FROM = 'depesz@depesz.com'
APP_ADMIN = 'depesz@depesz.com'
APP_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_FOLDER = APP_DIR + '/root'
# Default configuration

app = Flask(__name__, static_folder=STATIC_FOLDER, static_url_path='/static')
app.config.from_object(__name__)
app.config.from_envvar('KANASTARC', silent=True)

db_conn_details = {}
keys = ('database', 'user', 'password', 'host', 'port', 'sslmode')
for conn_key, conf_key in zip(keys, ['DB_%s' % i.upper() for i in keys]):
    if conf_key in app.config:
        db_conn_details[conn_key] = app.config[conf_key]

db = model.Model(db_conn_details)


if not app.debug:
    file_logger = logging.FileHandler(LOG_FILENAME)
    file_logger.setLevel(logging.INFO)
    app.logger.addHandler(file_logger)

    mail_logger = logging.handlers.SMTPHandler(MAIL_SERVER, MAIL_FROM, APP_ADMIN, 'Critical error in %s' % (__name__))
    mail_logger.setLevel(logging.CRITICAL)
    app.logger.addHandler(mail_logger)

    file_logger.setFormatter(
        logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s '
            '[in %(pathname)s:%(lineno)d]'
        )
    )

    mail_logger.setFormatter(
        logging.Formatter(
            '''
            Message type:       %(levelname)s
            Location:           %(pathname)s:%(lineno)d
            Module:             %(module)s
            Function:           %(funcName)s
            Time:               %(asctime)s

            Message:

            %(message)s
            '''
        )
    )


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'username' in session:
            return f(*args, **kwargs)
        flash('NOT_LOGGED')
        return redirect(url_for('index'))
    return decorated_function


@app.teardown_request
def teardown_request(exception):
    db.rollback()


@app.errorhandler(Exception)
def special_exception_handler(error):
    app.logger.critical('Fatal exception', exc_info=1)
    return render_template('error.html'), 500


@app.route('/main')
@login_required
def main():
    games = db.get_list_of_open_games()
    users = db.get_list_of_players()
    last_games = db.get_pair_games_metainfo()
    new_games = {}
    for g in last_games:
        new_games[g['id']] = [ g['pair1_player1'], g['pair1_player2'], g['pair2_player1'], g['pair2_player2'], g['second_dealing']]
        if g['first_dealing'] == g['pair1_player1']:
            new_games[g['id']].append(g['pair1_player2'])
        elif g['first_dealing'] == g['pair1_player2']:
            new_games[g['id']].append(g['pair1_player1'])
        elif g['first_dealing'] == g['pair2_player1']:
            new_games[g['id']].append(g['pair2_player2'])
        else:
            new_games[g['id']].append(g['pair2_player1'])
    return render_template('main.html', games=games, users=users, last_games=last_games, new_games=new_games)


@app.route('/logout')
def logout():
    for key in ('username', 'is_admin'):
        if key in session:
            session.pop(key)
    flash('LOGGED_OUT')
    return redirect(url_for('index'))


@app.route('/login', methods=['POST'])
def login():
    user_data = db.get_user_data(request.form['username'])

    if user_data is None or user_data["password"] != crypt.crypt(request.form['password'], user_data['password']):
        for key in ('username', 'is_admin'):
            if key in session:
                session.pop(key)
        app.logger.error('Bad login for user %s with password %s', request.form['username'], request.form['password'])
        flash('BAD_USER_PASSWORD')
        return redirect(url_for('index'))

    session['username'] = user_data['username']
    session['admin'] = user_data['is_admin']
    app.logger.warning('User %s logged in', user_data['username'])
    return redirect(url_for('main'))


@app.route('/')
def index():
    if 'username' in session:
        return redirect(url_for('main'))

    return render_template('index.html')


@app.route('/new_game', methods=['POST', 'GET'])
@login_required
def new_game():
    if request.method == 'POST' and new_game_validate():
        new_game_id = db.register_new_game(
            request.form['pair1_player1'],
            request.form['pair1_player2'],
            request.form['pair2_player1'],
            request.form['pair2_player2'],
            request.form['first_dealing'],
            request.form['second_dealing']
        )
        return redirect(url_for('game', game_id=new_game_id))

    users = db.get_list_of_players()

    return render_template('new_game.html', users=users)


def new_game_validate():
    required = ('pair1_player1', 'pair1_player2', 'pair2_player1', 'pair2_player2', 'first_dealing', 'second_dealing')
    for key in required:
        if key in request.form and request.form[key]:
            continue
        g.errors = ['MISSING_DATA']
        return False

    seen = {}
    for key in ('pair1_player1', 'pair1_player2', 'pair2_player1', 'pair2_player2'):
        if request.form[key] in seen:
            g.errors = ['DUPLICATE_DATA']
            return False
        seen[key] = 1

    if request.form['first_dealing'] == request.form['second_dealing']:
        g.errors = ['DUPLICATE_DATA']
        return False

    first_dealing_pair = 2
    second_dealing_pair = 2
    if request.form['first_dealing'] in (request.form['pair1_player1'], request.form['pair1_player2']):
        first_dealing_pair = 1
    if request.form['second_dealing'] in (request.form['pair1_player1'], request.form['pair1_player2']):
        second_dealing_pair = 1

    if first_dealing_pair == second_dealing_pair:
        g.errors = ['BAD_DEALING']
        return False

    return True

@app.route('/game/<int:game_id>/<int:deal_no>', methods=['GET', 'POST'])
@login_required
def deal(game_id, deal_no):
    if request.method == 'POST' and update_deal(game_id, deal_no):
        return redirect(url_for('game', game_id=game_id))

    (game, deal) = db.get_game_and_deal_details(game_id, deal_no)
    return render_template('deal.html', game=game, deal=deal )

def update_deal(game_id, deal_no):
    scores = {}
    for pair in ('p1', 'p2'):
        for player in ('p1', 'p2'):
            for place in ('hand', 'table'):
                partial_score_key = '%s%s_%s' % (pair, player, place)
                scores[ partial_score_key ] = 0
                if partial_score_key in request.form:
                    try:
                        partial_score = int(request.form[partial_score_key])
                        if place == "hand":
                            partial_score = -1 * abs(partial_score)
                        scores[ partial_score_key ] = partial_score
                    except ValueError:
                        scores[ partial_score_key ] = 0

    db.update_deal( game_id, deal_no, request.form['finished'], scores )
    return True

@app.route('/game/<int:game_id>', methods=['POST', 'GET'])
@login_required
def game(game_id):
    if request.method == 'POST' and new_deal_insert(game_id):
        return redirect(url_for('game', game_id=game_id))

    (game, deals) = db.get_game_details(game_id)

    pair1 = set([game['pair1_player1'], game['pair1_player2']])
    pair2 = set([game['pair2_player1'], game['pair2_player2']])
    dealing_order = []
    dealing_order.append(game['first_dealing'])
    dealing_order.append(game['second_dealing'])

    if game['first_dealing'] in pair1:
        pair1.remove(game['first_dealing'])
        pair2.remove(game['second_dealing'])
        dealing_order.extend(pair1)
        dealing_order.extend(pair2)
    else:
        pair2.remove(game['first_dealing'])
        pair1.remove(game['second_dealing'])
        dealing_order.extend(pair2)
        dealing_order.extend(pair1)

    return render_template('game.html', game=game, deals=deals, dealing_order=dealing_order)


def new_deal_insert(game_id):
    if 'finish_game' in request.form:
        db.finish_game(game_id)
        return True

    if not 'finished' in request.form:
        g.errors = ['NO_FINISHED']
        return False

    scores = {}
    for pair in ('p1', 'p2'):
        for player in ('p1', 'p2'):
            for place in ('hand', 'table'):
                partial_score_key = '%s%s_%s' % (pair, player, place)
                scores[ partial_score_key ] = 0
                if partial_score_key in request.form:
                    try:
                        partial_score = int(request.form[partial_score_key])
                        if place == "hand":
                            partial_score = -1 * abs(partial_score)
                        scores[ partial_score_key ] = partial_score
                    except ValueError:
                        scores[ partial_score_key ] = 0

    db.register_new_deal( game_id, request.form['finished'], scores )
    return True


@app.route("/games")
@login_required
def games():
    games = db.get_list_of_games()
    return render_template('games.html', games=games)


@app.route('/game_stats')
@login_required
def game_stats():
    games = {}
    deals = {}

    games['summary'] = db.get_game_stat('summary')
    games['massacres'] = db.get_game_stat('massacres')
    games['longest'] = db.get_game_stat('longest')
    games['surprises'] = db.get_game_stat('surprises')
    games['streaks'] = db.get_game_stat('streaks')
    games['callongest'] = db.get_game_stat('callongest')

    deals['worst'] = db.get_deal_stat('worst')
    deals['best'] = db.get_deal_stat('best')
    deals['massacres'] = db.get_deal_stat('massacres')
    return render_template('stats.html', games=games, deals=deals)


@app.route('/game-<int:game_id>.png')
@login_required
def game_graph(game_id):
    (game, deals) = db.get_game_details(game_id)

    x = [0] + [int(r['deal_no']) for r in deals]
    team1 = [0] + [int(r['pair1_sum']) for r in deals]
    team2 = [0] + [int(r['pair2_sum']) for r in deals]
    team_diff = [abs(i[0] - i[1]) for i in zip(team1, team2)]
    team1_label = '%s + %s' % (game['pair1_player1'], game['pair1_player2'])
    team2_label = '%s + %s' % (game['pair2_player1'], game['pair2_player2'])
    min_range = min(team1 + team2 + team_diff)
    max_range = max(team1 + team2 + team_diff)
    guide_lines = [i for i in (0, 2500, 5000, 10000) if min_range <= i <= max_range]

    fig = Figure(facecolor="white")
    ax = fig.add_subplot(111)

    ax.plot(x, team1, color='red', linestyle='-', linewidth=2, label=team1_label)
    ax.plot(x, team2, color='blue', linestyle='-', linewidth=2, label=team2_label)
    ax.plot(x, team_diff, color='magenta', linestyle='--', linewidth=1, label=u'różnica')
    for i in guide_lines:
        ax.axhline(y=i, linewidth=1, color='green')

    ax.get_xaxis().set_ticks(x)
    ax.legend(loc='upper left')
    ax.grid()

    png_output = io.BytesIO()
    FigureCanvas(fig).print_png(png_output)

    response = make_response(png_output.getvalue())
    response.headers['Content-Type'] = 'image/png'
    return response


@app.route('/robots.txt')
@app.route('/favicon.ico')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


if __name__ == '__main__':
    params = {}
    if 'HOST' in app.config:
        params['host'] = app.config['HOST']
    if 'PORT' in app.config:
        params['port'] = app.config['PORT']
    app.run(**params)

# vim: set ft=python:
