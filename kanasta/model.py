#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras
import os


class Model(object):

    def __init__(self, connection_data):
        self.db_conn_details = connection_data
        os.environ['PGAPPNAME'] = 'kanasta_web'
        self.db_conn = psycopg2.connect(connection_factory=psycopg2.extras.DictConnection, **self.db_conn_details)

    def cursor(self):
        return self.db_conn.cursor()

    def commit(self):
        return self.db_conn.commit()

    def rollback(self):
        return self.db_conn.rollback()

    def get_user_data(self, username):
        cur = self.cursor()
        cur.execute("SELECT * FROM users WHERE username = %s and password is not null and password <> ''", (username,))
        user_data = cur.fetchone()
        cur.close()
        return user_data

    def register_new_game(self, pair1_player1, pair1_player2, pair2_player1, pair2_player2, deal_1, deal_2):
        cur = self.cursor()

        if pair1_player1 < pair1_player2:
            p1p1 = pair1_player1
            p1p2 = pair1_player2
        else:
            p1p1 = pair1_player2
            p1p2 = pair1_player1

        cur.execute("""
            INSERT INTO games
                (started, pair1_player1, pair1_player2, pair2_player1, pair2_player2, first_dealing, second_dealing)
            VALUES
                (now(), least(%(p1_p1)s, %(p1_p2)s), greatest(%(p1_p1)s, %(p1_p2)s), least(%(p2_p1)s, %(p2_p2)s), greatest(%(p2_p1)s, %(p2_p2)s), %(deal_1)s, %(deal_2)s)
            RETURNING id""", {
                'p1_p1': pair1_player1,
                'p1_p2': pair1_player2,
                'p2_p1': pair2_player1,
                'p2_p2': pair2_player2,
                'deal_1': deal_1,
                'deal_2': deal_2,
            }
        )
        res = cur.fetchone()
        cur.close()
        self.commit()
        return res['id']

    def get_list_of_players(self):
        cur = self.cursor()
        cur.execute('SELECT username FROM users order by username')
        users_rs = cur.fetchall()
        cur.close()
        return [u['username'] for u in users_rs]

    def get_game_details(self, game_id):
        cur = self.cursor()
        cur.execute('SELECT * FROM games WHERE id = %s', (game_id,))
        game = cur.fetchone()
        cur.execute('select *, sum(pair1_score) over (order by deal_no) as pair1_sum, sum(pair2_score) over (order by deal_no) as pair2_sum from deals where game_id = %s order by deal_no', (game_id,))
        deals = cur.fetchall()
        cur.close()
        return (game, deals)

    def get_game_and_deal_details(self, game_id, deal_no):
        cur = self.cursor()
        cur.execute('SELECT * FROM games WHERE id = %s', (game_id,))
        game = cur.fetchone()
        cur.execute('select * from deals where game_id = %s and deal_no = %s', (game_id, deal_no,))
        deal = cur.fetchone()
        cur.close()
        return (game, deal)

    def finish_game(self, game_id):
        cur = self.cursor()
        cur.execute('update games set is_finished = true where id = %s', (game_id,))
        cur.close()
        self.commit()
        return

    def register_new_deal(self, game_id, ended_by, scores):
        cur = self.cursor()
        cur.execute('SELECT coalesce( max(deal_no), 0 ) as no from deals where game_id = %s', (game_id,))
        last_deal_no = cur.fetchone()['no']
        cur.execute(
                'INSERT INTO deals ( game_id, deal_no, ended_by, pair1_player1_hand, pair1_player1_table, pair1_player2_hand, pair1_player2_table, pair2_player1_hand, pair2_player1_table, pair2_player2_hand, pair2_player2_table) values ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                (
                    game_id, last_deal_no + 1, ended_by,
                    scores['p1p1_hand'], scores['p1p1_table'],
                    scores['p1p2_hand'], scores['p1p2_table'],
                    scores['p2p1_hand'], scores['p2p1_table'],
                    scores['p2p2_hand'], scores['p2p2_table'],
                )
        )

        cur.close()
        self.commit()
        return

    def update_deal(self, game_id, deal_no, ended_by, scores):
        cur = self.cursor()
        cur.execute(
                'UPDATE deals SET ended_by = %s, pair1_player1_hand = %s, pair1_player1_table = %s, pair1_player2_hand = %s, pair1_player2_table = %s, pair2_player1_hand = %s, pair2_player1_table = %s, pair2_player2_hand = %s, pair2_player2_table = %s WHERE game_id = %s AND deal_no = %s',
                (
                    ended_by,
                    scores['p1p1_hand'], scores['p1p1_table'],
                    scores['p1p2_hand'], scores['p1p2_table'],
                    scores['p2p1_hand'], scores['p2p1_table'],
                    scores['p2p2_hand'], scores['p2p2_table'],
                    game_id, deal_no,
                )
        )

        cur.close()
        self.commit()
        return

    def get_pair_games_metainfo(self):
        cur = self.cursor()
        cur.execute("select distinct on (pair1_player1, pair1_player2, pair2_player1, pair2_player2) * from games order by pair1_player1, pair1_player2, pair2_player1, pair2_player2, started desc")
        games = cur.fetchall()
        cur.close()
        return games

    def get_list_of_open_games(self):
        cur = self.cursor()
        cur.execute("SELECT * FROM games WHERE NOT is_finished ORDER BY started desc")
        games = cur.fetchall()
        cur.close()
        return games

    def get_list_of_games(self):
        cur = self.cursor()
        cur.execute("SELECT * FROM games ORDER BY started desc")
        games = cur.fetchall()
        cur.close()
        return games

    def get_game_stat(self, stat_type):
        cur = self.cursor()
        if stat_type == 'summary':
            cur.execute("""
            SELECT g.pair1_player1, g.pair1_player2, g.pair2_player1, g.pair2_player2,
                sum(case when g.pair1_score > g.pair2_score then 1 else 0 end) as pair1_wins,
                sum(case when g.pair1_score < g.pair2_score then 1 else 0 end) as pair2_wins,
                sum(case when g.pair1_score = g.pair2_score then 1 else 0 end) as draws,
                sum( x.pair1_deals_win ) as pair1_deals_win,
                sum( x.pair2_deals_win ) as pair2_deals_win,
                sum( x.deal_draws ) as deal_draws,
                sum( x.pair1_sum ) as pair1_sum,
                sum( x.pair2_sum ) as pair2_sum
            FROM games g
                left outer join (
                    select
                        d.game_id,
                        sum( case when d.pair1_score > d.pair2_score then 1 else 0 end ) as pair1_deals_win,
                        sum( case when d.pair1_score < d.pair2_score then 1 else 0 end ) as pair2_deals_win,
                        sum( case when d.pair1_score = d.pair2_score then 1 else 0 end ) as deal_draws,
                        sum( d.pair1_score) as pair1_sum,
                        sum( d.pair2_score) as pair2_sum
                    FROM
                        deals d
                    GROUP BY d.game_id
                ) as x on g.id = x.game_id
            WHERE g.is_finished = true
            GROUP BY g.pair1_player1, g.pair1_player2, g.pair2_player1, g.pair2_player2
            """)
        elif stat_type == 'massacres':
            cur.execute("""
            SELECT started, id, pair1_player1, pair1_player2, pair2_player1, pair2_player2, pair1_score, pair2_score, abs(pair1_score - pair2_score) as diff
            FROM games
            WHERE is_finished = true
            ORDER BY diff desc limit 10
            """)
        elif stat_type == 'longest':
            cur.execute("""
            SELECT g.*, (select max(d.deal_no) from deals d where d.game_id = g.id) as length
            FROM games g
            WHERE g.is_finished = true and exists (select * from deals d where d.game_id = g.id)
            ORDER BY length desc nulls last limit 10
            """)
        elif stat_type == 'streaks':
            cur.execute("""
            with base_data as (
                SELECT
                    id,
                    started,
                    pair1_player1,
                    pair1_player2,
                    pair2_player1,
                    pair2_player2,
                    case when pair1_score > pair2_score THEN 'pair1' ELSE 'pair2' END as winner
                FROM games WHERE is_finished
            ), add_winner_change as (
                SELECT
                    *,
                    case
                        when winner IS distinct FROM lag(winner) over (partition BY pair1_player1,pair1_player2,pair2_player1,pair2_player2 ORDER BY started)
                            THEN started
                        ELSE NULL
                    END as streak_start
                FROM
                    base_data
            ), fill_in_streak_starts as (
                SELECT
                    id, started, pair1_player1, pair1_player2, pair2_player1, pair2_player2, winner,
                    max( streak_start ) over (partition BY pair1_player1,pair1_player2,pair2_player1,pair2_player2 ORDER BY started) as streak_start
                FROM add_winner_change
            ), top_10 as (
                SELECT
                    streak_start,
                    pair1_player1, pair1_player2, pair2_player1, pair2_player2,
                    winner,
                    count(*) as win_count,
                    min(started) as min_started,
                    max(started) as max_started,
                    array_agg(id) as game_ids
                FROM
                    fill_in_streak_starts
                group BY streak_start, pair1_player1, pair1_player2, pair2_player1, pair2_player2, winner
                ORDER BY win_count desc, min_started asc LIMIT 10
            )
            SELECT
            *
            FROM
            top_10
            """)
        elif stat_type == 'surprises':
            cur.execute("""
                with game_scores as (
                    select
                        game_id,
                        deal_no,
                        sum(pair1_score) over (partition BY game_id ORDER BY deal_no) as pair1_c_score,
                        sum(pair2_score) over (partition BY game_id ORDER BY deal_no) as pair2_c_score
                    FROM
                        deals
                )
                SELECT
                    g.id,
                    g.started,
                    g.pair1_player1,
                    g.pair1_player2,
                    g.pair2_player1,
                    g.pair2_player2,
                    g.pair1_score,
                    g.pair2_score,
                    gs.deal_no,
                    abs(gs.pair1_c_score - gs.pair2_c_score) as diff
                FROM
                    game_scores gs join games g on gs.game_id = g.id
                WHERE
                    ( g.pair1_score < g.pair2_score ) <> ( gs.pair1_c_score < gs.pair2_c_score )
                ORDER BY diff desc LIMIT 10
            """)
        elif stat_type == 'callongest':
            cur.execute("""
                SELECT
                    g.id,
                    g.started,
                    g.last_deal_finished,
                    g.pair1_player1,
                    g.pair1_player2,
                    g.pair2_player1,
                    g.pair2_player2,
                    g.pair1_score,
                    g.pair2_score,
                    g.last_deal_finished - g.started as duration
                FROM
                    games g
                order by g.last_deal_finished  - g.started desc nulls last limit 10
            """)
        else:
            raise NameError('Unknown stat type: %s' % (stat_type))
        data = cur.fetchall()
        cur.close()
        return data

    def get_deal_stat(self, stat_type):
        cur = self.cursor()
        if stat_type == 'worst':
            cur.execute("""
                WITH d AS (
                    select game_id, deal_no, '1' as pair, pair1_score as score from deals
                    union all
                    select game_id, deal_no, '2' as pair, pair2_score as score from deals
                    order by score asc limit 10
            )
                SELECT g.*, d.*
                from d join games g on d.game_id = g.id
                ORDER BY d.score asc limit 10
            """)
        elif stat_type == 'best':
            cur.execute("""
                WITH d AS (
                    select game_id, deal_no, '1' as pair, pair1_score as score from deals
                    union all
                    select game_id, deal_no, '2' as pair, pair2_score as score from deals
                    order by score desc limit 10
            )
                SELECT g.*, d.*
                from d join games g on d.game_id = g.id
                ORDER BY d.score desc limit 10
            """)
        elif stat_type == 'massacres':
            cur.execute("""
                WITH d AS (
                    select game_id, deal_no, abs(pair1_score - pair2_score) as diff
                    from deals
                    order by diff desc limit 10
            )
                SELECT g.*, d.*
                from d join games g on d.game_id = g.id
                ORDER BY d.diff desc limit 10
            """)
        else:
            raise NameError('Unknown stat type: %s' % (stat_type))
        data=cur.fetchall()
        cur.close()
        return data

# vim: set ft=python:
