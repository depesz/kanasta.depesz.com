#!/usr/bin/env bash

cd "$( dirname "$0" )"

export PGAPPNAME=kanasta_web
export KANASTARC="$( pwd )/settings.prod.py"
export pid_file="/home/depesz/tmp/kanasta.pid"

case "$1" in
    start)
        listen="$( (cat settings.prod.py ; echo 'print("{}:{}".format(HOST, PORT))') | python3 )"
        gunicorn3 -w 2 -p "$pid_file" -D --log-file=log/gunicorn.log -b "$listen" kanasta:app
    ;;
    stop)
        cat "$pid_file" | xargs -r kill
    ;;
    reload)
        cat "$pid_file" | xargs -r kill -HUP
    ;;

    *)
        echo "Eeee? I know just start, stop and reload commands" >&2
        exit 1
esac

exit 0
