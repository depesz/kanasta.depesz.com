BEGIN;
SELECT _v.register_patch( 'more-deal-information', ARRAY[ 'games' ], NULL );

ALTER TABLE deals
    add column pair1_player1_hand INT4,
    add column pair1_player1_table INT4,
    add column pair1_player2_hand INT4,
    add column pair1_player2_table INT4,
    add column pair2_player1_hand INT4,
    add column pair2_player1_table INT4,
    add column pair2_player2_hand INT4,
    add column pair2_player2_table INT4,
    add column pair1_end_bonus INT4,
    add column pair2_end_bonus INT4,
    add column ended_by TEXT,
    add column dealed_on timestamptz,
    add column editable bool NOT NULL DEFAULT false,
    ALTER column dealed_on SET DEFAULT now(),
    ALTER column editable SET DEFAULT true,
    ADD CONSTRAINT deals_ended_by_fkey FOREIGN KEY (ended_by) REFERENCES users(username),
    add check (pair1_end_bonus IS NULL OR pair1_end_bonus = 300 ),
    add check (pair2_end_bonus IS NULL OR pair2_end_bonus = 300 ),
    add check ( (pair1_end_bonus IS NULL) OR (pair2_end_bonus IS NULL) )
;

CREATE OR REPLACE FUNCTION set_deal_pair_scores () RETURNS TRIGGER AS
$BODY$
DECLARE
    game_data record;
BEGIN
    NEW.pair1_end_bonus := NULL;
    NEW.pair2_end_bonus := NULL;
    IF NEW.ended_by IS NOT NULL THEN
        SELECT * INTO game_data FROM games WHERE id = NEW.game_id;
        IF NEW.ended_by = game_data.pair1_player1 OR NEW.ended_by = game_data.pair1_player2 THEN
            NEW.pair1_end_bonus := 300;
        ELSIF NEW.ended_by = game_data.pair2_player1 OR NEW.ended_by = game_data.pair2_player2 THEN
            NEW.pair2_end_bonus := 300;
        ELSE
            NEW.ended_by := NULL;
        END IF;
    END IF;

    NEW.pair1_score :=
        abs( coalesce( NEW.pair1_player1_hand, 0 ) ) * -1 +
        abs( coalesce( NEW.pair1_player2_hand, 0 ) ) * -1 +
        coalesce( NEW.pair1_player1_table, 0 ) +
        coalesce( NEW.pair1_player2_table, 0 ) +
        coalesce( NEW.pair1_end_bonus, 0 );

    NEW.pair2_score :=
        abs( coalesce( NEW.pair2_player1_hand, 0 ) ) * -1 +
        abs( coalesce( NEW.pair2_player2_hand, 0 ) ) * -1 +
        coalesce( NEW.pair2_player1_table, 0 ) +
        coalesce( NEW.pair2_player2_table, 0 ) +
        coalesce( NEW.pair2_end_bonus, 0 );

    RETURN NEW;
END;
$BODY$
LANGUAGE 'plpgsql';
CREATE TRIGGER set_deal_pair_scores BEFORE INSERT OR UPDATE ON deals FOR EACH ROW EXECUTE PROCEDURE set_deal_pair_scores();

CREATE OR REPLACE FUNCTION update_game_score_after_deal() RETURNS TRIGGER AS
$BODY$
DECLARE
    sums record;
BEGIN
    IF TG_OP = 'INSERT' THEN
        SELECT sum(pair1_score) as p1, sum(pair2_score) as p2 INTO sums FROM deals WHERE game_id = NEW.game_id;
        UPDATE games SET pair1_score = sums.p1, pair2_score = sums.p2 WHERE id = NEW.game_id;
    ELSIF TG_OP = 'DELETE' THEN
        SELECT sum(pair1_score) as p1, sum(pair2_score) as p2 INTO sums FROM deals WHERE game_id = OLD.game_id;
        UPDATE games SET pair1_score = sums.p1, pair2_score = sums.p2 WHERE id = OLD.game_id;
    ELSIF TG_OP = 'UPDATE' THEN
        SELECT sum(pair1_score) as p1, sum(pair2_score) as p2 INTO sums FROM deals WHERE game_id = NEW.game_id;
        UPDATE games SET pair1_score = sums.p1, pair2_score = sums.p2 WHERE id = NEW.game_id;
        IF NEW.game_id <> OLD.game_id THEN
            SELECT sum(pair1_score) as p1, sum(pair2_score) as p2 INTO sums FROM deals WHERE game_id = OLD.game_id;
            UPDATE games SET pair1_score = sums.p1, pair2_score = sums.p2 WHERE id = OLD.game_id;
        END IF;
    END IF;
    RETURN NULL;
END;
$BODY$
LANGUAGE 'plpgsql';
CREATE TRIGGER update_game_score_after_deal AFTER INSERT OR UPDATE OR DELETE ON deals FOR EACH ROW EXECUTE PROCEDURE update_game_score_after_deal();

COMMIT;
