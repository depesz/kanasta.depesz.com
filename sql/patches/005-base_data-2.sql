BEGIN;
    SELECT _v.register_patch( 'base_data-2', ARRAY[ 'base_data', 'games', 'users' ], NULL );

    INSERT INTO games ( started, pair1_player1, pair1_player2, pair1_score, pair2_player1, pair2_player2, pair2_score, first_dealing, second_dealing) VALUES
        (  '2012-03-22',  'ania',  'pawel',  '11235',  'depesz',  'ula',  '6595',  'ania', 'depesz' )
    ;
    INSERT INTO deals (game_id, deal_no, pair1_score, pair2_score) VALUES
        ( currval( 'games_id_seq'), 1, 1555, 2150 ),
        ( currval( 'games_id_seq'), 2, 2600, 1785 ),
        ( currval( 'games_id_seq'), 3, 2710, 1470 ),
        ( currval( 'games_id_seq'), 4, -530, -900 ),
        ( currval( 'games_id_seq'), 5, 3110, 225 ),
        ( currval( 'games_id_seq'), 6, 285, 1430 ),
        ( currval( 'games_id_seq'), 7, 1505, 435 )
    ;

    INSERT INTO games ( started, pair1_player1, pair1_player2, pair1_score, pair2_player1, pair2_player2, pair2_score, first_dealing, second_dealing) VALUES
        (  '2012-03-24',  'ania',  'pawel',  '4645',  'depesz',  'ula',  '10990',  'depesz', 'pawel' )
    ;
    INSERT INTO deals (game_id, deal_no, pair1_score, pair2_score) VALUES
        ( currval( 'games_id_seq'), 1, 1040, 2225 ),
        ( currval( 'games_id_seq'), 2, -720, 565 ),
        ( currval( 'games_id_seq'), 3, 3860, 1730 ),
        ( currval( 'games_id_seq'), 4, -715, 1390 ),
        ( currval( 'games_id_seq'), 5, -790, 2820 ),
        ( currval( 'games_id_seq'), 6, -30, -165 ),
        ( currval( 'games_id_seq'), 7, 870, 605 ),
        ( currval( 'games_id_seq'), 8, 1130, 1820 )
    ;

COMMIT;
