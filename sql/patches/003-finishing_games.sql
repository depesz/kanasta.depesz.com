BEGIN;
    SELECT _v.register_patch( 'finishing_games', ARRAY[ 'games' ], NULL );

    ALTER TABLE games ADD COLUMN is_finished bool;
    UPDATE games SET is_finished = true;
    ALTER TABLE games ALTER COLUMN is_finished SET NOT NULL;
    ALTER TABLE games ALTER COLUMN is_finished SET DEFAULT false;

COMMIT;
