BEGIN;
SELECT _v.register_patch( 'game-durations', ARRAY[ 'more-deal-information' ], NULL );

ALTER TABLE games add column last_deal_finished timestamptz;

UPDATE games as g
    SET last_deal_finished = x.last
    FROM ( SELECT game_id, max(dealed_on) as last FROM deals WHERE dealed_on IS NOT NULL group BY game_id ) x
    WHERE g.id = x.game_id;

with
    m as (SELECT game_id, min(dealed_on) as f FROM deals WHERE dealed_on IS NOT NULL group BY game_id),
    d as (SELECT g.id, g.started, m.f, m.f - g.started FROM games g join m on g.id = m.game_id WHERE g.started::date <> m.f::date)
UPDATE games as g SET started = d.f FROM d WHERE g.id = d.id;

CREATE OR REPLACE FUNCTION update_game_last_deal_ts() RETURNS TRIGGER AS
$BODY$
DECLARE
BEGIN
    UPDATE games SET last_deal_finished = greatest(last_deal_finished, NEW.dealed_on) WHERE id = NEW.game_id;
    RETURN NULL;
END;
$BODY$
LANGUAGE 'plpgsql';
CREATE TRIGGER update_game_last_deal_ts AFTER INSERT ON deals FOR EACH ROW EXECUTE PROCEDURE update_game_last_deal_ts();

COMMIT;
