BEGIN;
    SELECT _v.register_patch( 'games', ARRAY[ 'users' ], NULL );

    CREATE TABLE games (
        id serial PRIMARY KEY,
        started timestamptz NOT NULL DEFAULT now(),
        pair1_player1 TEXT references users (username) NOT NULL,
        pair1_player2 TEXT references users (username) NOT NULL,
        pair2_player1 TEXT references users (username) NOT NULL,
        pair2_player2 TEXT references users (username) NOT NULL,
        pair1_score INT4 NOT NULL DEFAULT 0,
        pair2_score INT4 NOT NULL DEFAULT 0,
        first_dealing TEXT references users (username) NOT NULL,
        second_dealing TEXT references users (username) NOT NULL,
        constraint  distinct_players               check ( pair1_player1 <> pair1_player2 AND pair1_player1 <> pair2_player1 AND pair1_player1 <> pair2_player2 AND pair1_player2 <> pair2_player1 AND pair1_player2 <> pair2_player2 AND pair2_player1 <> pair2_player2 ),
        constraint  sensible_first_dealing         check ( first_dealing in ( pair1_player1, pair1_player2, pair2_player1, pair2_player2 ) ),
        constraint  sensible_second_dealing        check ( second_dealing in ( pair1_player1, pair1_player2, pair2_player1, pair2_player2 ) ),
        constraint  first_dealers_different_pairs  check ((first_dealing in ( pair1_player1, pair1_player2 ) AND second_dealing in (pair2_player1, pair2_player2)) OR (second_dealing in ( pair1_player1, pair1_player2 ) AND first_dealing in (pair2_player1, pair2_player2))),
        constraint  pair1_players_ordered          check ( pair1_player1 < pair1_player2 ),
        constraint  pair2_players_ordered          check ( pair2_player1 < pair2_player2 ),
        constraint  pairs_ordered                  check ( pair1_player1 < pair2_player1 )
    );

    CREATE TABLE deals (
        game_id INT4 references games (id),
        deal_no INT4,
        pair1_score INT4 NOT NULL,
        pair2_score INT4 NOT NULL,
        PRIMARY KEY (game_id, deal_no),
        check ( deal_no >= 1 )
    );

COMMIT;
