BEGIN;
    SELECT _v.register_patch( 'users', NULL, NULL );

    CREATE TABLE users (
        username TEXT PRIMARY KEY,
        password TEXT,
        is_admin bool
    );
COMMIT;
