BEGIN;
    SELECT _v.register_patch( 'base_data', ARRAY[ 'games', 'users' ], NULL );

    INSERT INTO users (username, password, is_admin) VALUES ('depesz', '', true );
    INSERT INTO users (username, password, is_admin) VALUES ('ula', '', false );
    INSERT INTO users (username, password, is_admin) VALUES ('ania', '', false );
    INSERT INTO users (username, password, is_admin) VALUES ('pawel', '', false );

    INSERT INTO games ( started, pair1_player1, pair1_player2, pair1_score, pair2_player1, pair2_player2, pair2_score, first_dealing, second_dealing) VALUES
        (  '2011-08-20',  'ania',  'pawel',  '15040',  'depesz',  'ula',  '10160',  'ania', 'ula'  ),
        (  '2011-08-29',  'ania',  'pawel',  '9600',   'depesz',  'ula',  '11350',  'ania', 'ula'  ),
        (  '2011-09-03',  'ania',  'pawel',  '8640',   'depesz',  'ula',  '10020',  'ania', 'ula'  ),
        (  '2011-09-09',  'ania',  'pawel',  '9620',   'depesz',  'ula',  '11070',  'ania', 'ula'  ),
        (  '2011-09-10',  'ania',  'pawel',  '14440',  'depesz',  'ula',  '16560',  'ania', 'ula'  ),
        (  '2011-09-24',  'ania',  'pawel',  '10100',  'depesz',  'ula',  '5840',   'ania', 'ula'  ),
        (  '2011-09-25',  'ania',  'pawel',  '16690',  'depesz',  'ula',  '9840',   'ania', 'ula'  ),
        (  '2011-09-30',  'ania',  'pawel',  '10450',  'depesz',  'ula',  '6420',   'ania', 'ula'  ),
        (  '2011-10-07',  'ania',  'pawel',  '12410',  'depesz',  'ula',  '7690',   'ania', 'ula'  ),
        (  '2011-10-30',  'ania',  'pawel',  '12180',  'depesz',  'ula',  '9060',   'ania', 'ula'  ),
        (  '2011-11-11',  'ania',  'pawel',  '11200',  'depesz',  'ula',  '4280',   'ania', 'ula'  ),
        (  '2011-11-27',  'ania',  'pawel',  '6770',   'depesz',  'ula',  '10820',  'ania', 'ula'  ),
        (  '2011-12-23',  'ania',  'pawel',  '10660',  'depesz',  'ula',  '6930',   'ania', 'ula'  ),
        (  '2011-12-24',  'ania',  'pawel',  '7590',   'depesz',  'ula',  '10700',  'ania', 'ula'  ),
        (  '2011-12-26',  'ania',  'pawel',  '8140',   'depesz',  'ula',  '10620',  'ania', 'ula'  ),
        (  '2012-01-05',  'ania',  'pawel',  '5120',   'depesz',  'ula',  '10590',  'ania', 'ula'  ),
        (  '2012-01-14',  'ania',  'pawel',  '9795',   'depesz',  'ula',  '11920',  'ania', 'ula'  ),
        (  '2012-01-19',  'ania',  'pawel',  '12580',  'depesz',  'ula',  '6485',   'ania', 'ula'  ),
        (  '2012-01-20',  'ania',  'pawel',  '10910',  'depesz',  'ula',  '8605',   'ania', 'ula'  ),
        (  '2012-01-22',  'ania',  'pawel',  '6780',   'depesz',  'ula',  '10585',  'ania', 'ula'  ),
        (  '2012-01-28',  'ania',  'pawel',  '10150',  'depesz',  'ula',  '5505',   'ania', 'ula'  ),
        (  '2012-02-03',  'ania',  'pawel',  '6605',   'depesz',  'ula',  '10380',  'ania', 'ula'  ),
        (  '2012-02-09',  'ania',  'pawel',  '11765',  'depesz',  'ula',  '8590',   'ania', 'ula'  ),
        (  '2012-02-18',  'ania',  'pawel',  '10670',  'depesz',  'ula',  '9630',   'ania', 'ula'  ),
        (  '2012-02-25',  'ania',  'pawel',  '12720',  'depesz',  'ula',  '5585',   'ania', 'ula'  ),
        (  '2012-02-26',  'ania',  'pawel',  '11225',  'depesz',  'ula',  '10080',  'depesz', 'pawel'  )
    ;
    INSERT INTO deals (game_id, deal_no, pair1_score, pair2_score) VALUES
        ( currval( 'games_id_seq'), 1, 1230, 1915 ),
        ( currval( 'games_id_seq'), 2, 2280, 1780 ),
        ( currval( 'games_id_seq'), 3, 600, -1055 ),
        ( currval( 'games_id_seq'), 4, -110, 1535 ),
        ( currval( 'games_id_seq'), 5, 1380, 1230 ),
        ( currval( 'games_id_seq'), 6, -675, 1785 ),
        ( currval( 'games_id_seq'), 7, 470, -495 ),
        ( currval( 'games_id_seq'), 8, 1035, 325 ),
        ( currval( 'games_id_seq'), 9, 2165, 150 ),
        ( currval( 'games_id_seq'), 10, 1250, 1385 ),
        ( currval( 'games_id_seq'), 11, 1600, 1525 )
    ;
commit;
