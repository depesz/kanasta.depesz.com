CREATE user kanasta;
CREATE DATABASE kanasta WITH OWNER kanasta;

\echo 'Now, connect to the kanasta database, and load versioning'
\echo 'This can be done using following commands from within psql:'
\echo
\echo '\\c kanasta kanasta'
\echo '\\! git clone git://github.com/depesz/Versioning.git'
\echo '\\i Versioning/install.versioning.sql'
\echo
\echo 'Afterwards, create actual schema using \\i stable-full.sql'
