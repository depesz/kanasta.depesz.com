#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from kanasta import app

params = {}
if 'HOST' in app.config:
    params['host'] = app.config['HOST']
if 'PORT' in app.config:
    params['port'] = app.config['PORT']

app.run(**params)

# vim: set ft=python:
