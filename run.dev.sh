#!/usr/bin/env bash
# make sure that current dir is project top dir
this_script="${BASH_SOURCE[0]}"
script_directory="$( dirname "${this_script}" )"
work_dir="$( readlink -f "${script_directory}" )"
cd "$work_dir"
# make sure that current dir is project top dir

project_name=kanasta

# I use ssh-ident tool (https://github.com/ccontavalli/ssh-ident), so I should
# set some env variables.
ssh_ident_agent_env="${HOME}/.ssh/agents/agent-priv-$( hostname -s )"
[[ -e "${ssh_ident_agent_env}" ]] && . "${ssh_ident_agent_env}" > /dev/null

# Check if the session already exist, and if yes - attach, with no changes
tmux has-session -t "${project_name}" 2> /dev/null && exec tmux attach-session -t "${project_name}"

log_dir="${work_dir}/log"
[[ -d "${log_dir}" ]] || mkdir -p "${log_dir}"
for log_file in "${log_dir}"/{gunicorn,kanasta}.log
do
    [[ -f "${log_file}" ]] || touch "${log_file}"
done

tmux new-session -d -s "${project_name}" -n "logs" -c "${log_dir}"
tmux split-window -d -t "${project_name}:logs" -c "${log_dir}"

tmux new-window -d -t "${project_name}"
tmux new-window -d -t "${project_name}:99" -n "server" -c "${work_dir}"

tmux send-keys -t "${project_name}:logs.0" "tail -F gunicorn.log" Enter
tmux send-keys -t "${project_name}:logs.1" "tail -F kanasta.log" Enter
tmux send-keys -t "${project_name}:server" "export KANASTARC='${work_dir}/settings.dev.py'" Enter
tmux send-keys -t "${project_name}:server" "python3 kanasta.py" Enter

tmux attach-session -t "${project_name}"
